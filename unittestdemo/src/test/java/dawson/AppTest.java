package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void echo_returns5()
    {
        assertEquals("this test makes sure the echo method properly returns the input it is given", 5, App.echo(5));
    } 

    @Test
    public void oneMore_returns6()
    {
        assertEquals("this test makes sure the oneMore method properly returns the input it is given + 1", 6, App.oneMore(5));
    } 
}
